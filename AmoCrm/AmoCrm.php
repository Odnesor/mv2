<?php

namespace Amocrm;

class AmoCrm
{
	public static function init() {
		add_action( 'acf/init', [__CLASS__, 'add_options'] );
		add_action( 'rest_api_init', function () {
			register_rest_route( 'amocrm', 'token/expiration', array (
				'methods'  => 'GET',
				'callback' => [Credentials::getInstance(), 'checkRefeshTokenExpiration'],
			) );
		} );
	}

	public static function add_options() {
		if ( function_exists( 'acf_add_options_page' ) ) {
			Options::acf_add_options_page();
			Options::add_options_fields();
			Credentials::subscribeOnCredentialsUpdate( Options::AMOCRM_SETTINGS_GROUP_KEY );
		}
	}

	public static function saveToCrm( $data ) {
		if ( get_field( 'amo_crm_options', 'options' )['disabled'] ) return;
		Request::addContacts( $data );
	}
}