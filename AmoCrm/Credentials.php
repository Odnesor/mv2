<?php

namespace Amocrm;

use Amocrm\Exception\Token as TokenException;

class Credentials
{
	const ACCESS_TOKEN_META_KEY = 'amocrm_access_token';
	const REFRESH_TOKEN_META_KEY = 'amocrm_refresh_token';
	const REFRESH_TOKEN_EXPIRES_META_KEY = 'amocrm_refresh_token_expires';
	const CRM_FIELDS_IDS_META_KEY = 'amocrm_fields_ids';

	private static $_instance;
	private $crm_url;
	private $id;
	private $client_secret;
	private $code;
	private $redirect_uri;
	private $auth_token;
	private $refresh_token;
	private $refresh_token_expires;
	private $crm_fields_ids;


	public static function getInstance(): Credentials {
		self::$_instance = self::$_instance ?? new self;
		return self::$_instance;
	}

	public static function subscribeOnCredentialsUpdate($acf_key){
		add_filter("acf/update_value/key={$acf_key}", function($value, $post_id, $field, $original){
			self::$_instance = new self;
			self::$_instance->generateTokens();
			self::$_instance->getCrmFieldsIds();
			return $value;
		}, 100 , 4);
	}

	public function generateTokens(){
		try{
			if(!$this->crm_url) throw new TokenException('Missing crm url! Check admin settings!');
			$response = Request::getTokens();
			$this->updateTokensValues($response);
		}catch(TokenException | \Amocrm\Exception\Request $ex){
			$ex->log($ex->getMessage());
		}
	}

	public function refreshTokens(){
		try{
			if(!$this->refresh_token) $this->generateTokens();
			$response = Request::refreshTokens();
			$this->updateTokensValues($response);
		}catch(TokenException | \Amocrm\Exception\Request $ex){
			$ex->log($ex->getMessage());
		}
	}

	public function updateTokensValues($data){
		$this->auth_token = $data['access_token'];
		$this->refresh_token = $data['refresh_token'];
		$this->refresh_token_expires = strtotime('+4 weeks');
	}

	//is being inited by cron
	public function checkRefeshTokenExpiration(){
		if(time() > $this->refresh_token_expires){
			$this->refreshTokens();
		}
	}

	public function getCrmFieldsIds(){
		try{
			$fields_ids = [];
			$response = Request::getCrmColumns();
			if(!isset($response['_embedded']['custom_fields'])){
				throw new \Amocrm\Exception\Request('WROnG contacts crm request!');
			}
			array_walk($response['_embedded']['custom_fields'], function($field) use(&$fields_ids){
				switch ($field['name']){
					case 'Откуда Узнали':
						$key = 'from';break;
					case 'Телефон':
						$key = 'phone';break;
					case 'Канал обращения':
						$key = 'channel';break;
					case 'Комментарий':
						$key = 'comment';break;
					default: return $field;
				}
				$fields_ids[$key] = $field['id'];
				return $field;
			});
		}catch(\Amocrm\Exception\Request $ex){
			$ex->log($ex->getMessage());
		}finally{
			$this->crm_fields_ids = $fields_ids;
		}
	}

	public function __construct() {
		$admin_credentials = get_field('amo_crm_options', 'options')['integration_credentials'];
		$this->crm_url = get_field('amo_crm_options', 'options')['crm_url'] ?? '';
		$this->id = $admin_credentials['id'] ?? '';
		$this->client_secret = $admin_credentials['client_secret'] ?? '';
		$this->code = $admin_credentials['code'] ?? '';

		$this->redirect_uri = $admin_credentials['code'] ?? '';
		$this->auth_token = get_post_meta(1, self::ACCESS_TOKEN_META_KEY, true);
		$this->refresh_token = get_post_meta(1, self::REFRESH_TOKEN_META_KEY, true);
		$this->refresh_token_expires = get_post_meta(1, self::REFRESH_TOKEN_EXPIRES_META_KEY, true);

		$this->crm_fields_ids = json_decode(get_post_meta(1, self::CRM_FIELDS_IDS_META_KEY, true), true);
		$server_protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
		$this->redirect_uri = $server_protocol . $_SERVER['SERVER_NAME'];
	}

	public function __destruct() {
		update_post_meta(1,self::ACCESS_TOKEN_META_KEY, $this->auth_token);
		update_post_meta(1,self::REFRESH_TOKEN_META_KEY, $this->refresh_token);
		update_post_meta(1,self::REFRESH_TOKEN_EXPIRES_META_KEY, $this->refresh_token_expires);
		update_post_meta(1,self::CRM_FIELDS_IDS_META_KEY, json_encode($this->crm_fields_ids));
	}

	public function get_crm_url(): string{ return $this->crm_url;}
	public function get_id(): string{ return $this->id;}
	public function get_client_secret(): string{ return $this->client_secret;}
	public function get_code(): string{ return $this->code;}
	public function get_redirect_uri(): string{ return $this->redirect_uri;}
	public function get_auth_token(): string{ return $this->auth_token ;}
	public function get_refresh_token(): string{ return $this->refresh_token;}
	public function get_crm_fields_ids(): array{ return $this->crm_fields_ids;}
}