<?php

namespace Amocrm\Exception;

use Amocrm\Logger;
use Throwable;

class Request extends \Exception
{
	public $logger;

	public function __construct( $message = "", $code = 0, Throwable $previous = null ) {
		$this->logger = new Logger( 'request-errors-log' );
		parent::__construct( $message, $code, $previous );
	}

	public function log( $message ) {
		$this->logger->log( $message );
	}
}