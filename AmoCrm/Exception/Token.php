<?php

namespace Amocrm\Exception;

use Amocrm\Logger;
use Throwable;

class Token extends \Exception
{
	public $logger;

	public function __construct( $message = "", $code = 0, Throwable $previous = null ) {
		$this->logger = new Logger( 'token-errors' );
		$message = 'TOKEN ERROR: ' . $message;
		parent::__construct( $message, $code, $previous );
	}

	public function log( $message ) {
		$this->logger->log( $message );
	}
}