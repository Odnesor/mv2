<?php

namespace Amocrm;

class Options
{
	const AMOCRM_SETTINGS_GROUP_KEY = 'field_amocrm61ebfd3103e79';

	public static function acf_add_options_page(){
		acf_add_options_page( array (
			'page_title' => 'AMO CRM',
			'menu_title' => 'AMO CRM',
			'menu_slug'  => 'amo-crm-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
		) );
	}

	public static function add_options_fields(){
		if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array(
				'key' => 'group_amocrm61ebfd02afe8a',
				'title' => 'AmoCrm',
				'fields' => array(
					array(
						'key' => self::AMOCRM_SETTINGS_GROUP_KEY,
						'label' => '',
						'name' => 'amo_crm_options',
						'type' => 'group',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'layout' => 'block',
						'sub_fields' => array(
							array(
								'key' => 'field_amocrm61ec0e6175ea9',
								'label' => 'Crm url',
								'name' => 'crm_url',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'translations' => 'translate',
							),
							array(
								'key' => 'field_amocrm61ebfd4303e7b',
								'label' => 'Доступа интеграции',
								'name' => 'integration_credentials',
								'type' => 'group',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '80',
									'class' => '',
									'id' => '',
								),
								'layout' => 'row',
								'sub_fields' => array(
									array(
										'key' => 'field_amocrm61ebfd9003e7d',
										'label' => 'Id',
										'name' => 'id',
										'type' => 'text',
										'instructions' => '',
										'required' => 0,
										'conditional_logic' => 0,
										'wrapper' => array(
											'width' => '',
											'class' => '',
											'id' => '',
										),
										'default_value' => '',
										'placeholder' => '',
										'prepend' => '',
										'append' => '',
										'maxlength' => '',
										'translations' => 'translate',
									),
									array(
										'key' => 'field_amocrm61ebfdc003e7e',
										'label' => 'Секретный ключ',
										'name' => 'client_secret',
										'type' => 'text',
										'instructions' => '',
										'required' => 0,
										'conditional_logic' => 0,
										'wrapper' => array(
											'width' => '',
											'class' => '',
											'id' => '',
										),
										'default_value' => '',
										'placeholder' => '',
										'prepend' => '',
										'append' => '',
										'maxlength' => '',
										'translations' => 'translate',
									),
									array(
										'key' => 'field_amocrm61ebfdf203e7f',
										'label' => 'Код авторизации',
										'name' => 'code',
										'type' => 'text',
										'instructions' => '',
										'required' => 0,
										'conditional_logic' => 0,
										'wrapper' => array(
											'width' => '',
											'class' => '',
											'id' => '',
										),
										'default_value' => '',
										'placeholder' => '',
										'prepend' => '',
										'append' => '',
										'maxlength' => '',
										'translations' => 'translate',
									),
								),
							),
							array(
								'key' => 'field_amocrm61ebfd6703e7c',
								'label' => 'Отключить?',
								'name' => 'disabled',
								'type' => 'true_false',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '20',
									'class' => '',
									'id' => '',
								),
								'message' => '',
								'default_value' => 0,
								'ui' => 0,
								'translations' => 'copy_once',
								'ui_on_text' => '',
								'ui_off_text' => '',
							),
							array(
								'key' => 'field_amocrmtable61ebfd4303e7b',
								'label' => '',
								'name' => 'table_fields',
								'type' => 'group',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '80',
									'class' => '',
									'id' => '',
								),
								'layout' => 'table',
								'sub_fields' => array(
									array(
										'key' => 'field_amocrm_channel1ebfd9003e7d',
										'label' => 'Канал обращения',
										'name' => 'channel',
										'type' => 'text',
										'instructions' => '',
										'required' => 0,
										'conditional_logic' => 0,
										'wrapper' => array(
											'width' => '50',
											'class' => '',
											'id' => '',
										),
										'default_value' => '',
										'placeholder' => '',
										'prepend' => '',
										'append' => '',
										'maxlength' => '',
										'translations' => 'translate',
									),
									array(
										'key' => 'field_amocrm_from61ebfd9003e7d',
										'label' => 'Откуда узнали',
										'name' => 'from',
										'type' => 'text',
										'instructions' => '',
										'required' => 0,
										'conditional_logic' => 0,
										'wrapper' => array(
											'width' => '50',
											'class' => '',
											'id' => '',
										),
										'default_value' => $_SERVER['SERVER_NAME'],
										'placeholder' => '',
										'prepend' => '',
										'append' => '',
										'maxlength' => '',
										'translations' => 'translate',
									),
								),
							),
						),
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'amo-crm-settings',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

		endif;
	}
}