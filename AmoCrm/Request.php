<?php

namespace Amocrm;

use Amocrm\Exception\Token as TokenException;
use http\Exception;

class Request
{
	const EXCEPTIONS_MAP = [
		'110' => TokenException::class,
		'103' => \Amocrm\Exception\Request::class
	];

	public static function getTokens() {
		$data = [
			'client_id'     => Credentials::getInstance()->get_id(),
			'client_secret' => Credentials::getInstance()->get_client_secret(),
			'grant_type'    => 'authorization_code',
			'code'          => Credentials::getInstance()->get_code(),
			'redirect_uri'  => Credentials::getInstance()->get_redirect_uri()
		];
		$route = '/oauth2/access_token';
		return self::sendPostRequest( $data, $route );
	}

	public static function refreshTokens() {
		$data = [
			'client_id'     => Credentials::getInstance()->get_id(),
			'client_secret' => Credentials::getInstance()->get_client_secret(),
			'grant_type'    => 'refresh_token',
			'refresh_token' => Credentials::getInstance()->get_refresh_token(),
			'redirect_uri'  => Credentials::getInstance()->get_redirect_uri()
		];
		$route = '/oauth2/access_token';
		return self::sendPostRequest( $data, $route );
	}

	public static function addContacts( $data = [] ) {
		try {
			$from = get_field( 'amo_crm_options', 'options' )['table_fields']['from'] ?? '';
			$channel = get_field( 'amo_crm_options', 'options' )['table_fields']['channel'] ?? '';
			$utm = implode(PHP_EOL,array_map(function($utm_key){
				return $_COOKIE[$utm_key] ?? '';
			}, ['utm_source', 'utm_medium', 'utm_campaign', 'utm_content']));

			$lead['request']['contacts']['add'] = [
				[
					'name'          => $data['name'],
					'custom_fields' => [
						[
							'id'     => Credentials::getInstance()->get_crm_fields_ids()['phone'],
							'values' => [['value' => $data['phone'],
										  'enum'  => 'WORK']]
						],
						[
							'id'     => Credentials::getInstance()->get_crm_fields_ids()['from'],
							'values' => [['value' => $from]]
						],
						[
							'id'     => Credentials::getInstance()->get_crm_fields_ids()['channel'],
							'values' => [['value' => $channel]]
						],
						[
							'id'     => Credentials::getInstance()->get_crm_fields_ids()['comment'],
							'values' => [['value' => $utm]]
						],
					]
				]
			];
			$route = 'private/api/v2/json/contacts/set';
			self::sendPostRequest( $lead, $route );
		} catch (TokenException $ex) {
			$ex->log( $ex->getMessage() );
			Credentials::getInstance()->refreshTokens();
			self::addContacts();
		} catch (\Amocrm\Exception\Request $ex) {

		}
	}

	public static function getCrmColumns() {
		$route = '/api/v4/contacts/custom_fields';
		return self::sendGetRequest( $route );
	}

	public static function sendPostRequest( $request_data, $route ) {
		$url = Credentials::getInstance()->get_crm_url() . $route;
		$auth_token = Credentials::getInstance()->get_auth_token();


		$ch = curl_init();
		if ( ! $ch ) return false;

		$params = [
			CURLOPT_URL            => $url,
			CURLOPT_POST           => true,
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_USERAGENT      => 'amoCRM-API-client/1.0',
			CURLOPT_HTTPHEADER     => [
				'Content-Type: application/json',
				'Authorization: Bearer ' . $auth_token
			],
			CURLOPT_POSTFIELDS     => json_encode( $request_data ),
		];
		curl_setopt_array( $ch, $params );


		$raw = curl_exec( $ch );
		$http_status_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
		$response_data = json_decode( $raw, true );

		(new Logger('requests'))->log($request_data);
		(new Logger('requests'))->log($request_data);

		if (
			isset( $response_data['response']['error_code'] ) &&
			isset( self::EXCEPTIONS_MAP[ $response_data['response']['error_code'] ] )
		) {
			$exception_class = self::EXCEPTIONS_MAP[ $response_data['response']['error_code'] ];
			$message = $response_data['response']['error'];
			throw new $exception_class( $message );
		} elseif ( $http_status_code !== 200 ) {
			throw new \Amocrm\Exception\Request();
		}

		return $response_data;
	}

	public static function sendGetRequest( $route ) {
		$url = Credentials::getInstance()->get_crm_url() . $route;
		$auth_token = Credentials::getInstance()->get_auth_token();

		$params = [
			CURLOPT_URL            => $url,
			CURLOPT_POST           => false,
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_USERAGENT      => 'amoCRM-API-client/1.0',
			CURLOPT_HTTPHEADER     => [
				'Content-Type: application/json',
				'Authorization: Bearer ' . $auth_token
			],
		];


		$ch = curl_init();
		if ( ! $ch ) return false;

		curl_setopt_array( $ch, $params );
		$raw = curl_exec( $ch );
		$http_status_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
		$response_data = json_decode( $raw, true );

		return $response_data;
	}
}