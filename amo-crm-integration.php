<?php

class Amo_Crm_Integration
{
	public static function init() {
		$classes = glob( __DIR__ . '/AmoCrm/*.php' );
		array_walk( $classes, function ( $class ) {
			include_once $class;
		} );
		$classes = glob( __DIR__ . '/AmoCrm/Exception/*.php' );
		array_walk( $classes, function ( $class ) {
			include_once $class;
		} );
		\Amocrm\AmoCrm::init();
	}
}